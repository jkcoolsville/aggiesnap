
//
// This is a GUI support code to the chapters 12-16 of the book
// "Programming -- Principles and Practice Using C++" by Bjarne Stroustrup
//

#ifndef SIMPLE_WINDOW_GUARD
#define SIMPLE_WINDOW_GUARD 1

#include "GUI.h"    // for Simple_window only (doesn't really belong in Window.h)
#include <FL/Fl_Check_Button.H>
#include "Graph.h"

using namespace Graph_lib;



//------------------------------------------------------------------------------

struct Search_window : Graph_lib::Window {
	Search_window(Point xy, int w, int h, const string& title);
private:
	Button exit_button;
	Button search_button2;
	Check_Button check1;
	Check_Button check2;
	Check_Button check3;
	Check_Button check4;
	Check_Button check5;
	
	
	
	static void cb_back2(Address, Address);
	void back2();
	
	static void cb_search2(Address, Address);
	void search2();
	
};

struct Simple_window : Graph_lib::Window {
    Simple_window(Point xy, int w, int h, const string& title );

    bool wait_for_button(); // simple event loop
private:
    Button exit_button;     // the "next" button
	Button cont_button;
	Button add_button;
	Button url_button;
	Button back_button;
	Button forward_button;
	Button retry_button;
	Button search_button;
	Check_Button check1;
	Check_Button check2;
	Check_Button check3;
	Check_Button check4;
	Check_Button check5;
	Text t;
	Out_box counter;
	int current = 0;
	int count = 0;//Number of times back is clicked
	In_box add_box;
	In_box file_box;
	Image splash;
	Vector_ref<Image> entry;
	Vector_ref <Image> pics;
    bool button_pushed;     // implementation detail
	Search_window win2;

	
	
    static void cb_exit(Address, Address); // callback for next_button
    void exit();            // action to be done when next_button is pressed
	
	static void cb_cont(Address, Address);
	void cont();
	
	static void cb_add(Address, Address);
	void add();
	
	static void cb_url(Address, Address);
	void url();
	
	static void cb_back(Address, Address);
	void back();
	
	static void cb_forward(Address, Address);
	void forward();
	
	static void cb_retry(Address, Address);
	void retry();
	
	static void cb_search(Address, Address);
	void search();
	
};

//------------------------------------------------------------------------------

#endif // SIMPLE_WINDOW_GUARD
