Project: AggieSnap
Team 53 //Team Name TBA
Due Date: on December 3, 2014

Goal: Using FLTK and C++11 we need to create a database program with a GUI.

1.(Josh)	Get a picture from a filename.  (The file might have come from a cellphone camera.)  You only need to accept file extensions of jpg, gif, jpeg, JPG, GIF, and JPEG (unless you want extra creditsee below).  Allow the user to recover from entering an incorrect type of file, a file that can't be opened, etc.


2.(Josh)	Get a picture from a URL, using wget as follows:
		system((string("wget ") + URLstring + " O " + filename_string).c_str());
to download from the URLstring website to a file whose name is in filename_string.  (See the man pages for more information.)  You only need to accept file extensions of jpg, gif, jpeg, JPG, GIF, and JPEG (unless you want extra creditsee below).  Allow the user to recover from entering an incorrect file extension, a URL that can't be downloaded, etc.


3.	Add up to 5 tags per picture (Family, Friends, Aggieland, Pets, Vacation, or any combination of these).


4.	Save the picture to disk, and save information to a picture index file.  The picture index file should consist of human-readable text; for example, lines like
	(reveilleVIII.jpg,Aggieland,Pets,,,)
since there are only two tags on that picture.  This file should be read each time the program is run, so that adding pictures is cumulative.  Hint: See the textbook's code for printing Date objects.


5.(Luke)	Browse all pictures.  When a picture is displayed, so are its tags.  Provide buttons to see the Next or the Previous picture.


6.(Luke)	Find pictures with any combination of tags.  Provide buttons to see the Next or the Previous picture with those tags.

Easiest Extra Credit Tasks:

1.	Accept two additional picture formats FLTK supports (bm, bmp, pbm, pgm, png, ppm, xbm, xpm, and their upper-case versions).


2.	Allow the user to change the text of the tag labels and save the new information to the picture index file in place of the old.


3.	Allow the user to delete a picture from the picture index file.  (Be sure to rewrite the picture index file.)