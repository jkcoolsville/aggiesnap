/*
   Simple_window.cpp
   Minimally revised for C++11 features of GCC 4.6.3 or later
   Walter C. Daugherity		June 10, 2012
*/

//
// This is a GUI support code to the chapters 12-16 of the book
// "Programming -- Principles and Practice Using C++" by Bjarne Stroustrup
//

#include "Simple_window.h"
#include "std_lib_facilities_4.h"
using namespace Graph_lib;

Search_window::Search_window(Point xy, int w, int h, const string& title) :
	Window(xy,w,h,title),
	exit_button(Point(x_max()-70,0), 70, 20, "Exit", cb_exit),
	check1(Point(60,450), 15, 15, "TAMU"),
	check2(Point(210,450), 15, 15, "Family"),
	check3(Point(360,450), 15, 15, "Friends"),
	check4(Point(510,450), 15, 15, "School"),
	check5(Point(660,450), 15, 15, "Social"),
	search_button2(Point(250,500),300,100,"Search", cb_search2)
	
{
	attach(check1);
	attach(check2);
	attach(check3);
	attach(check4);
	attach(check5);
	attach(exit_button);
	attach(search_button2);
}


void Search_window::cb_exit(Address, Address pw)
{
	reference_to<Search_window>(pw).exit();
}

void Search_window::cb_search2(Address, Address pw)
{
	reference_to<Search_window>(pw).search2();
}

void Search_window::exit()
{
    hide();
}

void Search_window::search2()
{
    hide();
}

//------------------------------------------------------------------------------

Simple_window::Simple_window(Point xy, int w, int h, const string& title) :
    Window(xy,w,h,title),
    //splash {Point{0,0}, "Starting Image.jpg"},
	splash {Point{0,0}, "aggiesnap.jpg"},
	exit_button(Point(x_max()-70,0), 70, 20, "Exit", cb_exit),
	cont_button(Point(0,0), x_max(), y_max(), "Continue", cb_cont),
	check1(Point(60,450), 15, 15, "TAMU"),
	check2(Point(210,450), 15, 15, "Family"),
	check3(Point(360,450), 15, 15, "Friends"),
	check4(Point(510,450), 15, 15, "School"),
	check5(Point(660,450), 15, 15, "Social"),
	add_button(Point(500,0), 40, 20, "File", cb_add),
	url_button(Point(540,0), 40, 20, "URL", cb_url),
	add_box(Point(100,0),400,20, "URL or Name"),
	file_box(Point(100,20),400,20, "URL File Name"),
	back_button(Point(0,500),100,100, "@<-", cb_back),
	forward_button(Point(700,500),100,100, "@->", cb_forward),
	retry_button(Point(250,270),100,20,"Retry",cb_retry),
	t(Point(150,250), "Error with your image, please type in another file name and click \"Retry\""),
	counter(Point(400,400),50,20, "Picture: "),
	search_button(Point(250,500),100,100,"Search", cb_search),
	win2(Point(150,50),800,600,"Search"),
    button_pushed(false)
{
	splash.resize(x_max(),y_max());
	attach(splash);
	win2.hide();
	attach(exit_button);
	attach(cont_button);
}

//------------------------------------------------------------------------------

bool Simple_window::wait_for_button()
// modified event loop:
// handle all events (as per default), quit when button_pushed becomes true
// this allows graphics without control inversion
{
    show();
    button_pushed = false;
#if 1
    // Simpler handler
    while (!button_pushed) Fl::wait();
    Fl::redraw();
#else
    // To handle the case where the user presses the X button in the window frame
    // to kill the application, change the condition to 0 to enable this branch.
    Fl::run();
#endif
    return button_pushed;
}

//------------------------------------------------------------------------------

void Simple_window::cb_exit(Address, Address pw)
// call Simple_window::exit() for the window located at pw
{  
    reference_to<Simple_window>(pw).exit();    
}

//------------------------------------------------------------------------------

void Simple_window::cb_cont(Address, Address pw)
{  
    reference_to<Simple_window>(pw).cont();    
}
//------------------------------------------------------------------------------

void Simple_window::cb_add(Address, Address pw)
{  
    reference_to<Simple_window>(pw).add();    
}
//------------------------------------------------------------------------------
void Simple_window::cb_url(Address, Address pw)
{  
    reference_to<Simple_window>(pw).url();    
}
//------------------------------------------------------------------------------
void Simple_window::cb_back(Address, Address pw)
{
	reference_to<Simple_window>(pw).back();
}

//------------------------------------------------------------------------------
void Simple_window::cb_forward(Address, Address pw)
{
	reference_to<Simple_window>(pw).forward();
}
//------------------------------------------------------------------------------
void Simple_window::cb_retry(Address, Address pw)
{
	reference_to<Simple_window>(pw).retry();
}
//------------------------------------------------------------------------------

void Simple_window::cb_search(Address, Address pw)
{
	reference_to<Simple_window>(pw).search();
}

//------------------------------------------------------------------------------

void Simple_window::exit()
{
    //button_pushed = true;
    hide();
}
//------------------------------------------------------------------------------
void Simple_window::cont()
{
	detach(cont_button);
	detach(splash);
	attach(add_button);
	attach(add_box);
	attach(url_button);
	attach(file_box);
	attach(check1); attach(check2); attach(check3); attach(check4); attach(check5);
	attach(back_button);
	attach(forward_button);
	attach(search_button);
	attach(counter);
}
//------------------------------------------------------------------------------
void Simple_window::add()
{
	if(count >= 1)
	{
		detach(pics[pics.size()-1]);
		redraw();
		//Add something that will remove the picture from the vector ref if a Bad picture
	}
	string name = add_box.get_string();
	entry.push_back(new Image(Point(50,50),name));
	if(entry[entry.size()-1].Image::is_bad(name)==false)
	{
		attach(t);
		attach(retry_button);
		redraw();
	}
	else {
		count++; current = count;
		pics.push_back(new Image{Point(50,50),name});
		pics[pics.size()-1].resize(x_max()-100,350);
		attach(pics[pics.size()-1]);
		ostringstream ss;
		ss << current << '/' << count;
		counter.put(ss.str());
		redraw();
	}
	
	
}
//------------------------------------------------------------------------------
void Simple_window::url()
{
	string site = add_box.get_string();
	string filename_string = file_box.get_string();
	system((string("wget ") + site + " -O " + filename_string).c_str());
	//system((string("wget --spider ") + site).c_str());//Checks if the website exists
	if(count >= 1)
	{
		detach(pics[pics.size()-1]);
		redraw();
	}
	entry.push_back(new Image(Point(50,50),filename_string));
	if(entry[entry.size()-1].Image::is_bad(filename_string)==false)
	{
		attach(t);
		attach(retry_button);
		redraw();
	}
	else{
		count++; current = count;
		pics.push_back(new Image{Point(50,50),filename_string});
		pics[pics.size()-1].resize(x_max()-100,350);
		attach(pics[pics.size()-1]);
		ostringstream ss;
		ss << current << '/' << count;
		counter.put(ss.str());
		redraw();
	}
}
//------------------------------------------------------------------------------
void Simple_window::back()
{
	if(pics.size()-1>0)
	{
		if(current>1)
		{
			detach(pics[current-1]);
			attach(pics[current-2]);
			current--;
			ostringstream ss;
			ss << current << '/' << count;
			counter.put(ss.str());
			redraw();
		}
		else
		{
			detach(pics[0]);			
			attach(pics[pics.size()-1]);
			current = count;
			ostringstream ss;
			ss << current << '/' << count;
			counter.put(ss.str());
			redraw();
		}
	}
}
//------------------------------------------------------------------------------
void Simple_window::forward()
{
	if(pics.size()-1 > 0)
	{
		if(current-1<pics.size()-1)
		{
			detach(pics[current-1]);
			attach(pics[current]);
			current++;
			ostringstream ss;
			ss << current << '/' << count;
			counter.put(ss.str());
			redraw();
		}
		else
		{
			detach(pics[pics.size()-1]);			
			attach(pics[0]);
			current = 1;
			ostringstream ss;
			ss << current << '/' << count;
			counter.put(ss.str());
			redraw();
		}
	}
}
void Simple_window::retry()
{
	detach(t);
	detach(retry_button);
	redraw();
	add();
}

void Simple_window::search()
{
	win2.show();
}

